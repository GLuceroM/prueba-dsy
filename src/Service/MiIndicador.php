<?php

namespace App\Service;

use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use function PHPUnit\Framework\throwException;

class MiIndicador{

    const URL = 'https://mindicador.cl/api/';
    const DOLAR = 'dolar/';

    private $client;

    public function __construct( HttpClientInterface $client){
        $this->client = $client;
    }

    public function getDolarFromMonthYear( int $month, int $year ) {
        $values = [];
        try{
            $days = date('t', mktime(0, 0, 0, $month, 1, $year));
            for( $i = 1; $i <= $days; $i++ ){

                $url = self::URL . self::DOLAR . str_pad($i, 2, "0", STR_PAD_LEFT) . "-" . $month . "-" . $year;
                $response = $this->client->request('GET', $url );
                if( $response->getStatusCode() === 200 ){
                    $content = $response->toArray();
                    $values[$i] = isset($content['serie'][0]) ? $content['serie'][0]['valor'] : "";
                }
                else{
                    throw new Exception("Error al recorrer el mes");
                }
            }
            return [ 'error' => false, 'values' => $values ];
        }
        catch( \Exception $e ){
            return [ 'error' => true, 'message' => $e->getMessage() ];
        }
    }

}