<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class IndicatorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('month', ChoiceType::class, [
                'label' => "Mes",
                'choices'  => [ 
                    "Enero" =>'01', "Febrero"=>'02', "Marzo" =>'03', "Abril"=>'04', 
                    "Mayo" =>'05', "Junio"=>'06', "Julio" =>'07', "Agosto"=>'08', 
                    "Septiembre" =>'09', "Octubre"=>'10', "Noviembre" =>'11', "Diciembre"=>'12' ],
                'data' => date('m')
            ])
            ->add('year', ChoiceType::class, [
                'label' => "Año",
                'choices' => array_combine( range(date("Y"),1984), range(date("Y"),1984) ),
                'data' => date('Y')
            ])
            ->add('action', ChoiceType::class, [
                'label' => "Acción",
                'choices' => ['Mostrar' => 'show','Descargar Excel' => 'download'],
                'data' => 'show'
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }

}
