<?php

namespace App\Controller;

use App\Form\IndicatorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\MiIndicador;

class IndicatorController extends AbstractController
{

    /**
     * @Route("/", name="indicator")
     */
    public function index( Request $request, MiIndicador $indicador ): Response
    {
        $values = null;
        $monthName = null;
        $form = $this->createForm( IndicatorType::class );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            // TODO: fix locale
            setlocale(LC_ALL, 'es_CL');
            $monthName = strftime("%B", mktime(0, 0, 0, $data['month'],1,$data['year']));
            $result = $indicador->getDolarFromMonthYear($data['month'],$data['year']);

            // TODO: fix download csv file

        }

        return $this->renderForm('indicator/index.html.twig', [
            'form' => $form,
            'result' => $result,
            'monthName' => $monthName
        ]);
    }

}
