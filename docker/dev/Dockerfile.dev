# La imagen es la oficial soportada por php y contiene Apache 2.4 sobre Debian.
FROM php:7.4-apache

# Instalar dependencias necesarias para Debian.
RUN apt-get update && apt-get install -y \
wget \
libzip-dev \
zip \
git \
libxml2-dev \
iputils-ping \
default-mysql-client \
nano \
vim

# Activa modulos apache.
RUN a2enmod rewrite headers

# Agrega extenciones de docker para php.
RUN docker-php-ext-configure zip \
&& docker-php-ext-install zip \
&& docker-php-ext-install soap \
&& docker-php-ext-install intl \
&& docker-php-ext-install bcmath \
&& docker-php-ext-install pdo_mysql \ 
&& docker-php-ext-install mysqli 

# Instalar composer.
RUN curl -sS https://getcomposer.org/installer | php \
&& mv composer.phar /usr/local/bin/ \
&& ln -s /usr/local/bin/composer.phar /usr/local/bin/composer

RUN curl -sS https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

# Agrega el php.ini.
ADD docker/dev/php/php.ini /usr/local/etc/php

# Copia el proyecto.
COPY --chown=www-data:www-data . /var/www/html/

WORKDIR /var/www/html/

# Agrega el virtual host, desactiva el por defecto y activa el de aqmarket.
ADD docker/dev/apache/web.dev.conf /etc/apache2/sites-available/
RUN a2ensite web.dev.conf && a2dissite 000-default.conf

# Permisos
RUN chmod -R 755 /var/www/html/

# Expone el puerto y deja apache corriendo por debajo.
EXPOSE 80