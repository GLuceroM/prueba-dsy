AQmarket Docker V 1.1
=====================
# Iniciar primera vez.
```sh
./build_dev.sh
```
# Iniciar desarrollo.
```sh
./start_dev.sh

# Conectarse a MySQL del contenedor e importar base de datos.
```
HOST: localhost
PORT: 3308
USER: root
PASS: aqm_dev
```
La bd quedara dentro de docker/db/data de forma que siempre que se levante el contenedor cargue esa data.

Carga backup de forma directa:
```
cat /ruta/al/archivo.sql | docker exec -i aqmarket_mysql_1 /usr/bin/mysql -u root --password=aqm_dev aqmarket
```
donde **aqmarket_mysql_1** es el nombre del contenedor (ver en consola con el comando: docker ps)
